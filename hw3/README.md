Created by: Jack Langston (SP 2018)
Course: CSCI-B365

In order for the code to work, the files are intended to be executed in the following order:

 -- 1.2.R
 -- 4.R
 -- 5.R

This is due to interdependences in the files.

All further explanation is covered thoroughly in the file comments or in the assigment PDF.
