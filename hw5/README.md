To reproduce the results found in the report, execute the files in this order:

- knn.R
- crossValidation.R
- knnValidation.R
- naiveBayes.R
